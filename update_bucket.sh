#!/bin/bash

if [ ! $# == 2 ]
then
    echo "Usage: $0 FULL_PATH_TO_SOURCE_DIR NAME_OF_THE_BUCKET"
    exit
fi

cd $1; ls -1 | xargs -n1 -P10 -I% s3cmd -H sync $1/%/ s3://$2/%/
